require "active_support/concern"
require "active_support/string"
require "versioned_attributes/save_handler"

module VersionedAttributes
  module Immutability
    extend ::ActiveSupport::Concern

    included do
      @@versioned_attributes = []
      @@ancestor_attribute = :original_record
    end

    def before_save(record)
      SaveHandler.new(@@versioned_attributes, @@ancestor_attribute).before_save(record)
      super
    end

    def after_save(record)
      SaveHandler.new(@@versioned_attributes, @@ancestor_attribute).after_save(record)
      super
    end

    module ClassMethods
      @@versioned_attributes = []
      @@ancestor_attribute = :original_record

      # Makes listed field keys immutable
      def immutable(*attrs)
        attrs ||= []
        @@versioned_attributes = (@@versioned_attributes + attrs).map {|attr| attr.to_sym }.uniq
      end

      def ancestor_container(attr)
        @@ancestor_attribute = attr.to_sym unless attr.nil? or attr.to_s.blank?
        attr_accessor @@ancestor_attribute
      end
    end
  end
end
