module VersionedAttributes
  class SaveHandler
    def initialize(versioned_attributes, original_record_attribute)
      @versioned_attrs = versioned_attributes
      @original_attr = original_record_attribute
    end

    # save as new record with same attributes
    def before_save(record)
      unless changed_attributes_of(record).empty?
        new_original(record, database_copy_of(record))
        record.send("#{record.class.primary_key}=", nil)
        @new_record = true
        record.send(:clear_timestamp_attributes)
      end
    end

    # Deprecate old record if possible
    def after_save(record)
      if original_of(record)
        record.send(:deprecate_old_record) if record.respond_to? :deprecate_old_record, true
        original_of(record).save! if primary_key_of original_of(record)
      end
    end

    protected

    def new_original(record, new_original_record)
      record.send(:write_attribute, [@original_attr.to_sym, new_original_record])
    end

    def original_of(record)
      record.send(@original_attr)
    end

    # Get closest ancestor's primary key
    def primary_key_of(record)
      id = record.send(record.class.primary_key)
      id ||= primary_key_of original_of(record) if original_of(record)
    end

    # Get latest saved copy of record's ancestor
    #
    # return:
    #   - non-dirty copy of ancestor as it appears in the database
    #   - nil if no ancestor in database
    def database_copy_of(record)
      id = primary_key_of record
      record.class.find(id) unless id.nil?
    end

    # An array of the immutable attributes that have
    # had attempts to change them
    def changed_attributes_of(record)
      record.changed? ? (record.changed & @versioned_attrs) : []
    end
  end
end
