require "active_support/concern"
require "active_support/string"
require "versioned_attributes/version"
require "versioned_attributes/save_handler"
require "versioned_attributes/immutability"

module VersionedAttributes
  extend ::ActiveSupport::Concern

  included do
    extend Immutability
  end
end
