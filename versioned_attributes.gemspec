# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'versioned_attributes/version'

Gem::Specification.new do |spec|
  spec.name          = "versioned_attributes"
  spec.version       = VersionedAttributes::VERSION
  spec.authors       = ["David Alexander"]
  spec.email         = ["davidpaulalexander@gmail.com"]
  spec.homepage      = "https://github.com/thelonelyghost/versioned_attributes"
  spec.license       = "MIT"
  spec.summary       = %q{Enforces immutability for specified attributes of an ActiveRecord object by saving as a new record.}
  spec.description   = <<-EOF
    ActiveRecord objects are required.
  EOF

  spec.files         = `git ls-files -z`.split("\x0").grep(%r{^(bin|lib|test|spec|features)/})
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.required_ruby_version = '>= 1.9.3'
  spec.add_runtime_dependency "activesupport", "~> 4.0"
  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake"

  spec.cert_chain = ['certs/thelonelyghost.pem']
  spec.signing_key = File.expand_path("~/.ssh/gem-private_key.pem") if $0 =~ /gem\z/
end
