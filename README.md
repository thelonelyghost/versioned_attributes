# VersionedAttributes

TODO: Write a gem description

## Installation

Add this line to your application's Gemfile:

    gem 'versioned_attributes'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install versioned_attributes

## Security

VersionedAttributes is cryptographically signed. To be sure the gem you install hasn't been tampered with:

Add my public key (if you haven't already) as a trusted certificate

```console
$ gem cert --add <(curl -Ls https://raw.github.com/thelonelyghost/versioned_attributes/master/certs/thelonelyghost.pem)

$ gem install versioned_attributes -P MediumSecurity
```

The *MediumSecurity* trust profile will verify signed gems, but allow the installation of unsigned dependencies.

This is necessary because not all of VersionedAttributes' dependencies are signed, so we cannot use *HighSecurity*.

## Usage

TODO: Write usage instructions here

## Contributing

1. Fork it ( https://github.com/thelonelyghost/versioned_attributes/fork )
2. Create your feature branch (`git checkout -b feature/my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin feature/my-new-feature`)
5. Create a new Pull Request
